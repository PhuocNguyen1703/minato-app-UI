const images = {
    logo: require('~/assets/images/logo.svg').default,
    coverAvatar: require('~/assets/images/cover-avatar.jpg'),
};

export default images;
