const routes = {
    home: '/',
    profile: '/profile',
    register: '/register',
    login: '/login',
    chat: '/chat',
    employee: '/employee',
    checkin: '/checkin',
    candidates: '/candidates',
    email: '/email',
    private: '/private-chat',
    todo: '/todo',
    calendar: '/calendar',
    kanban: '/kanban',
};

export default routes;
